<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Companie;
use App\Employee;

class BasicController extends Controller
{
    public function create(Request $request)
    {
    	// dd($request->all());
    	if($request->hasfile('logo')){
    		$file = $request->file('logo');
    		$filename = $file->getClientOriginalName();
    		$file->move(public_path("/"),$filename);
    	}
    	$companies = new Companie();

    	$companies->name = $request->name;
    	$companies->email = $request->email;
    	$companies->logo = $filename;
    	$companies->website = $request->website;

    	$companies->save();
    	return response()->json($companies);
    }

    public function index(Request $request,$id = NULL)
    {
    	$companie_details = Companie::whereId($id)->first();

    	return response()->json($companie_details);
    }

    public function store(Request $request)
    {
    	// dd($request->all());
    	if($request->hasfile('logo')){
    		$file = $request->file('logo');
    		$filename = $file->getClientOriginalName();
    		$file->move(public_path("/"),$filename);
    	}

    	$companie_details = [
    		'name' => $request->name,
    		'email' => $request->email,
    		'logo' => $filename,
    		'website' => $request->website
    	];

    	Companie::whereId($request->id)->update($companie_details);

    	return response()->json($companie_details);
    }

    public function createEmp(Request $request)
    {
    	
    	$emp = new Employee();

    	$emp->first_name = $request->first_name;
    	$emp->last_name = $request->last_name;
    	$emp->company = $request->company;
    	$emp->email = $request->email;
    	$emp->phone = $request->phone;

    	$emp->save();
    	return response()->json($emp);
    }

    public function empIndex(Request $request,$id = NULL)
    {
    	$emp_details = Employee::whereId($id)->first();

    	return response()->json($emp_details);
    }

    public function empStore(Request $request)
    {
    	$emp_details = [
    		'first_name' => $request->first_name,
    		'last_name' => $request->last_name,
    		'company' => $request->company,
    		'email' => $request->email,
    		'phone' => $request->phone
    	];

    	Employee::whereId($request->id)->update($emp_details);

    	return response()->json($emp_details);
    }
}
