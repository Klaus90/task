<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companie extends Model
{
    protected $fillable = [
		'name',
		'email',
		'logo',
		'website',
		'created_by',
		'updated_by',
	];
}
