<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/companies','BasicController@create');
Route::get('/show-companies/{id?}','BasicController@index');
Route::post('/update-companies','BasicController@store');
Route::post('/emp','BasicController@createEmp');
Route::get('/show-emp/{id?}','BasicController@empIndex');
Route::post('/update-emp','BasicController@empStore');


// Route::post('/companies','BasicController@create');
// Route::get('/show-companies','BasicController@index');
// Route::get('/update-companies','BasicController@store');